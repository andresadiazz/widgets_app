import 'package:flutter/material.dart';

class MenuItems {
  final String title;
  final String subtitle;
  final String link;
  final IconData icon;

  const MenuItems(
      {required this.title,
      required this.subtitle,
      required this.link,
      required this.icon});
}

const appMenuItems = <MenuItems>[
  MenuItems(
      title: 'Counter Screen',
      subtitle: 'Contador',
      link: '/counter',
      icon: Icons.add),
  MenuItems(
      title: 'Botones',
      subtitle: 'Varios botones en flutter',
      link: '/buttons',
      icon: Icons.smart_button_outlined),
  MenuItems(
      title: 'Tarjetas',
      subtitle: 'Un contenedor de tarjetas',
      link: '/cards',
      icon: Icons.credit_card),
  MenuItems(
      title: 'ProgressIndicator',
      subtitle: 'Generales y controlados',
      link: '/progress',
      icon: Icons.refresh_rounded),
  MenuItems(
      title: 'SnackBars n Dialogs',
      subtitle: 'Indicadores en pantallas',
      link: '/snackbars',
      icon: Icons.refresh_rounded),
  MenuItems(
      title: 'Animated Container',
      subtitle: 'Stateful widget animado',
      link: '/animated',
      icon: Icons.check_box_outlined),
  MenuItems(
      title: 'UI Controls + Tiles',
      subtitle: 'Serie de controles de Flutter',
      link: '/uiControls',
      icon: Icons.car_rental_outlined),
  MenuItems(
      title: 'Introduccion a la app',
      subtitle: 'Pequeño tutorial introductorio',
      link: '/tutorial',
      icon: Icons.accessible_rounded),
  MenuItems(
      title: 'Infinite Scroll y Pull',
      subtitle: 'Listas infinitas',
      link: '/infinite',
      icon: Icons.list_alt_rounded),
  MenuItems(
      title: 'Cambiar tema',
      subtitle: 'Cambiar tema de la aplicacion',
      link: '/theme-changer',
      icon: Icons.color_lens_outlined),
];
