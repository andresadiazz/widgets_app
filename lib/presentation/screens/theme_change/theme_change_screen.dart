import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:widgets_app/presentation/providers/theme_provider.dart';

class ThemeChangeScreen extends ConsumerWidget {
  static const name = 'theme_change_screen';

  const ThemeChangeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final isDarkMode = ref.watch(themeNotifierProvider).isDarkmode;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Counter Screen'),
          actions: [
            IconButton(
                onPressed: () {
                  // ref.read(isDarkmodeProvider.notifier).state = !isDarkMode;
                  ref.read(themeNotifierProvider.notifier).toggleDarkmode();
                },
                icon: Icon(!isDarkMode
                    ? Icons.light_mode_rounded
                    : Icons.dark_mode_rounded))
          ],
        ),
        body: const _ThemeChangeView());
  }
}

class _ThemeChangeView extends ConsumerWidget {
  const _ThemeChangeView();

  @override
  Widget build(BuildContext context, ref) {
    final List<Color> colors = ref.watch(colorListProvider);
    final selectedColor = ref.watch(themeNotifierProvider).selectedColor;
    return ListView.builder(
      itemCount: colors.length,
      itemBuilder: (BuildContext context, int index) {
        final color = colors[index];
        return RadioListTile(
          title: Text(
            'Ese color',
            style: TextStyle(color: color),
          ),
          subtitle: Text('${color.value}'),
          activeColor: color,
          value: index,
          groupValue: selectedColor,
          onChanged: (value) {
            // TODO: notificar cambio
            ref.read(themeNotifierProvider.notifier).changeColor(index);
          },
        );
      },
    );
  }
}
