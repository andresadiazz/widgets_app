import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SlideInfo {
  final String title;
  final String caption;
  final String imageUrl;

  SlideInfo(this.title, this.caption, this.imageUrl);
}

final slides = <SlideInfo>[
  SlideInfo('Busca la comida', 'Fugiat do irure enim et id magna.',
      'assets/Images/1.png'),
  SlideInfo('Entrega rapida', 'Nostrud ullamco Lorem nostrud ad ex amet id.',
      'assets/Images/2.png'),
  SlideInfo(
      'Disfruta la comida',
      'Excepteur enim veniam laboris duis Lorem labore anim id proident.',
      'assets/Images/3.png')
];

class AppTutorialScreen extends StatefulWidget {
  static const name = 'tutorial_screen';

  const AppTutorialScreen({Key? key}) : super(key: key);

  @override
  State<AppTutorialScreen> createState() => _AppTutorialScreenState();
}

class _AppTutorialScreenState extends State<AppTutorialScreen> {
  final pageViewController = PageController();

  bool endReached = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageViewController.addListener(() {
      final page = pageViewController.page ?? 0;
      if (!endReached && page >= (slides.length - 1.5)) {
        setState(() {
          endReached = true;
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            PageView(
              controller: pageViewController,
              physics: const BouncingScrollPhysics(),
              children: slides.map((e) => _Slide(e)).toList(),
            ),
            Positioned(
                top: 50,
                right: 20,
                child: TextButton(
                  child: const Text('Salir'),
                  onPressed: () => context.pop(),
                )),
            if (endReached)
              Positioned(
                  bottom: 30,
                  right: 20,
                  child: FadeInRight(
                    from: 15,
                    delay: const Duration(seconds: 1),
                    child: FilledButton(
                        onPressed: () {
                          context.pop();
                        },
                        child: const Text('Comenzar')),
                  ))
          ],
        ));
  }
}

class _Slide extends StatelessWidget {
  final SlideInfo slideInfo;
  const _Slide(this.slideInfo);

  @override
  Widget build(BuildContext context) {
    final titleStyle = Theme.of(context).textTheme.titleLarge;
    final captionStyle = Theme.of(context).textTheme.bodySmall;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image(
              image: AssetImage(slideInfo.imageUrl),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              slideInfo.title,
              style: titleStyle,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              slideInfo.caption,
              style: captionStyle,
            ),
          ],
        ),
      ),
    );
  }
}
