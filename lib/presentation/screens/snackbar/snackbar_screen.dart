import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SnackbarScreen extends StatelessWidget {
  static const name = 'snackbar_screen';

  const SnackbarScreen({Key? key}) : super(key: key);

  void showCustomSnackbar(BuildContext context) {
    ScaffoldMessenger.of(context).clearSnackBars();
    final snackbar = SnackBar(
      content: const Text('Hola mundo'),
      action: SnackBarAction(
        label: 'Ok',
        onPressed: () {},
      ),
      duration: const Duration(seconds: 2),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }

  void openDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          elevation: 5,
          title: const Text('Estas seguro'),
          content: const Text(
              'Occaecat cupidatat fugiat non adipisicing dolore culpa exercitation est voluptate anim ex magna amet. Irure deserunt qui veniam consectetur eu do aliquip cupidatat pariatur anim tempor et dolor. Culpa exercitation cillum magna nisi. Laborum commodo pariatur culpa et. Et deserunt dolore velit aliquip nisi ex deserunt nisi aliquip. Cillum nulla id excepteur ut laboris qui esse consequat commodo.'),
          actions: [
            TextButton(
                onPressed: () => context.pop(), child: const Text('Cancelar')),
            FilledButton(onPressed: () {}, child: const Text('Aceptar'))
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Snackbars n Dialogs'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FilledButton(
                onPressed: () {
                  showAboutDialog(context: context, children: [
                    const Text(
                        'Anim non irure consectetur irure deserunt. Aute quis pariatur esse labore occaecat adipisicing proident cillum cupidatat consequat labore. Incididunt officia ipsum veniam pariatur culpa enim mollit aute deserunt velit voluptate. In ex et et et in laboris sint occaecat labore. Veniam laboris dolor laboris excepteur anim. Aute cupidatat ex velit Lorem reprehenderit ad exercitation. Fugiat quis nostrud aliqua velit ut.')
                  ]);
                },
                child: const Text('Licencias usadas')),
            FilledButton(
                onPressed: () => openDialog(context),
                child: const Text('Mostrar Dialogo')),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => showCustomSnackbar(context),
        label: const Text('Mostrar Snackbar'),
        icon: const Icon(Icons.remove_red_eye_outlined),
      ),
    );
  }
}
